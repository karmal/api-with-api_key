<?php

use App\Http\Controllers\Api\UsersController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::Resource('/users', UsersController::class)->middleware('auth.apikey');
// Route::get('/user', 'UsersController@edit');
// Route::get('/users', function(){
//     $person = [
//         'first name' => 'john',
//         'last name' => 'maliszewski'
//     ];
//     return $person;  
// });

// Route::get('/user/{user}', function (User $user){
//     return $user;
// });