@extends('layouts.app')
@section('content')

<form method="post" action="{{ route('store') }}">  
    @csrf     
           <div class="form-group">      
               <label for="firstname">First Name:</label><br/><br/>  
               <input type="text" class="form-control" name="firstname"/><br/><br/>  
               @if ($errors->has('firstname'))
                <span class="text-danger">{{ $errors->first('firstname') }}</span>
                @endif
           </div>  
            <div class="form-group">      
                <label for="fsurname">Surname:</label><br/><br/>  
               <input type="text" class="form-control" name="surname"/><br/><br/>  
               @if ($errors->has('surname'))
               <span class="text-danger">{{ $errors->first('surname') }}</span>
           @endif 
           </div>  
            <div class="form-group">      
               <label for="date_of_birth">DOB:</label><br/><br/>  
               <input type="text" class="form-control" name="date_of_birth"/><br/><br/> 
               @if ($errors->has('date_of_birth'))
                    <span class="text-danger">{{ $errors->first('date_of_birth') }}</span>
                @endif 
           </div>  
            <div class="form-group">      
               <label for="phone_number">Phone Number:</label><br/><br/>  
               <input type="text" class="form-control" name="phone_number"/><br/><br/>
               @if ($errors->has('phone_number'))
                    <span class="text-danger">{{ $errors->first('phone_number') }}</span>
                @endif   
           </div>  
           <div class="form-group">      
             <label for="email">Email:</label><br/><br/>  
             <input type="text" class="form-control" name="email"/><br/><br/>  
             @if ($errors->has('email'))
                <span class="text-danger">{{ $errors->first('email') }}</span>
            @endif 
         </div> 
 <br/>  
 <button type="submit" class="btn-btn" >Submit</button>  
 </form>  

 @endsection
 