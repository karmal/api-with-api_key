@extends('layouts.app')
@section('content')
<table class="w-100">  
    <thead>  
    <tr>  
    <td>  
    ID </td>  
    <td>  
    First Name </td>  
    <td>  
    Surname </td>  
    <td>  
    DOB </td>  
    <td>  
    Phone Number </td>  
    <td>  
    Email </td> 
    </tr>  
    </thead>  
    <tbody>  
    @foreach($users as $user)  
            <tr>  
                <td>{{$user->id}}</td>  
                <td>{{$user->firstname}}</td>  
                <td>{{$user->surname}}</td>  
                <td>{{$user->date_of_birth}}</td>  
                <td>{{$user->phone_number}}</td>  
                <td>{{$user->email}}</td>  
    <td >  
    <form action="{{ route('destroy', $user->id)}}" method="post">  
                      @csrf  
                      @method('DELETE')  
                      <button class="btn btn-danger" type="submit">Delete</button>  
                    </form>  
    </td>  
    <td >  
    <form action="{{ route('edit', $user->id)}}" method="GET">  
                      @csrf  
                       
                      <button class="btn btn-info" type="submit">Edit</button>  
                    </form>  
    </td>  
      
             </tr>  
    @endforeach  
    </tbody>  
    </table>  

@endsection


