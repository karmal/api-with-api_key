@extends('layouts.app')
@section('content')

<h1 class="text-center">Info for User :{{$user->firstname}} </h1>
<table class="table">
    <thead>
      <tr>
        <th scope="col">ID</th>
        <th scope="col">First Name</th>
        <th scope="col">Surname</th>
        <th scope="col">DOB</th>
        <th scope="col">Phone Number</th>
        <th scope="col">Email</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>{{$user->id}}</td>  
        <td>{{$user->firstname}}</td>  
        <td>{{$user->surname}}</td>  
        <td>{{$user->date_of_birth}}</td>  
        <td>{{$user->phone_number}}</td>  
        <td>{{$user->email}}</td> 
      </tr>

    </tbody>
  </table>

  @endsection
                
