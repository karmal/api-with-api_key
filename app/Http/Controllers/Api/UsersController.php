<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth.apikey');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return response()->json(['status' => 'success', 'users' => $users]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd('were here');
            $request->validate([
                'firstname' => 'required',
                'surname' => 'required',
                'date_of_birth' => 'required|date_format:m/d/Y',
                'phone_number' => 'required|digits:10',
                'email' => 'required|email',
            ]);
    
            $newUser = User::create($request->all());
    
            return response()->json(['status' => 'user successfully added to database', 'newUser' => $newUser]);
   

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $user = User::find($user);
        return response()->json(['status' => 'success', 'user' => $user]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([  
            'firstname' => 'required',
            'surname' => 'required',
            'date_of_birth' => 'required|date_format:m/d/Y',
            'phone_number' => 'required|digits:10',
            'email' => 'required|email', 
        ]);  
  
        $user = User::find($id);  
        $user->firstname =  $request->get('firstname');  
        $user->surname = $request->get('surname');  
        $user->date_of_birth = $request->get('date_of_birth');  
        $user->phone_number = $request->get('phone_number');  
        $user->email = $request->get('email');  
        
        $user->save(); 
        return response()->json(['status' => 'user successfully updated', 'user' => $user]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);  
        $user->delete();  
        $users = User::all();
        return response()->json(['status' => 'user successfully deleted', 'users' => $users]);
    }
}
