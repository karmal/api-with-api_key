<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    use HasFactory;

    protected $fillable = ['firstname','surname', 'date_of_birth', 'phone_number', 'email'];
    // protected $fillable = ['_token'];
}
